//Define an angular module for our app
var app = angular.module('myApp', []);

app.controller('userController', function($scope, $http) {
  getUser();
  function getUser(){
  $http.get("http://localhost/user/1").success(function(data){
        $scope.user = data;
       });
  };

  $scope.editUser = function (item) {
    $http.post("http://localhost/user/"+item).success(function(data){
        getUser();
        $scope.userInput = "";
      });
  };

});

# README #

### What is this repository for? ###

* Skoove web
* Version 0.0.1 (In progress)
* Will use [Web Api](https://bitbucket.org/peku2455/skooveapi)

### How do I get set up? ###

* `cd docker && docker-compose up -d`
* is dependent of api (if not used docker_php will not be up)
* the repo use angular 1.2 (index_old.html) but also angular 2.2 (tried to migrate to angular 2.2 but not enough time - index.html)

### Contribution guidelines ###

* No tests
* The code is in place but the request to api are not working
'use strict';

const express = require('express');
var morgan = require('morgan');
var fs = require('fs');
// Constants
const PORT = 8080;

// App
const app = express();
app.use(express.static(__dirname + '/js'));
app.use(morgan('dev'))

/*app.get('/js/!*', function(req, res) {
    if (req.files)
        res.sendfile(req.files);
});
app.get('/css/!*', function(req, res) {
    if (req.files)
        res.sendfile(req.files);
});*/

app.get('*', function(req, res) {
    console.log(req.url);
    if (req.file === undefined) {
        res.sendfile('./public/index_old.html'); // load the single view file (angular will handle the page changes on the front-end)
    }
});



app.listen(PORT);
console.log('Running on http://localhost:' + PORT);